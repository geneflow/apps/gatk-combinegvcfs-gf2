GATK CombineGVCFs GeneFlow App
==============================

Version: 4.1.4.0-02

This GeneFlow app wraps the GATK CombineGVCFs tool.

Inputs
------

1. input: Directory that contains g.vcf files.

2. reference_sequence: Directory that contains reference sequence fasta, index, and dict files.

Parameters
----------

1. output: Output directory - The name of the output directory to place the merged g.vcf file.

